## Preparation

`npm install`

## Compile contract

```
solcjs --bin --abi --include-path node_modules/ --base-path . contracts/NftTrade.sol -o build
```
