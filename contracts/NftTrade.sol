// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";

type OrderId is uint256;

contract NftTrade {
    enum State {
        Created,
        Locked
    }

    struct Order {
        address seller;
        address buyer;
        IERC20 erc20Token;
        IERC721 nftToken;
        uint256 nftId;
        uint256 erc20Price;
        State state;
    }

    address public owner;
    OrderId nextOrderId;
    mapping(OrderId => Order) public orders;

    event NewOwner(address newOwner);
    event OfferCreated(
        address erc20Token,
        address nftToken,
        uint256 nftId,
        uint256 erc20Price,
        OrderId orderId
    );
    event OfferRetracted(OrderId orderId);
    event OrderPlaced(OrderId orderId, address buyer);
    event OrderExecuted(OrderId orderId);
    event OrderRefunded(OrderId orderId);

    modifier inState(OrderId orderId, State expectedState) {
        require(orders[orderId].state == expectedState, "Invalid state.");
        _;
    }

    modifier onlySeller(OrderId orderId) {
        require(
            orders[orderId].seller == msg.sender,
            "Only the seller of the order can perform this action."
        );
        _;
    }

    modifier onlyBuyer(OrderId orderId) {
        require(
            orders[orderId].buyer == msg.sender,
            "Only the buyer of the order can perform this action."
        );
        _;
    }

    modifier onlyOwner() {
        require(msg.sender == owner, "Only owner can perform this action.");
        _;
    }

    constructor() {
        owner = msg.sender;
        nextOrderId = OrderId.wrap(1);
    }

    function setOwner(address newOwner) public onlyOwner {
        owner = newOwner;
        emit NewOwner(newOwner);
    }

    function createOffer(
        address erc20Token,
        address nftToken,
        uint256 nftId,
        uint256 erc20Price
    ) public returns (OrderId) {
        Order memory newOrder = Order({
            seller: msg.sender,
            buyer: address(0),
            erc20Token: IERC20(erc20Token),
            nftToken: IERC721(nftToken),
            nftId: nftId,
            erc20Price: erc20Price,
            state: State.Created
        });

        OrderId currentOrderId = nextOrderId;
        orders[currentOrderId] = newOrder;

        nextOrderId = OrderId.wrap(OrderId.unwrap(nextOrderId) + 1);

        emit OfferCreated(
            erc20Token,
            nftToken,
            nftId,
            erc20Price,
            currentOrderId
        );

        return currentOrderId;
    }

    function retractOffer(
        OrderId orderId
    ) public onlySeller(orderId) inState(orderId, State.Created) {
        delete orders[orderId];

        emit OfferRetracted(orderId);
    }

    function placeOrder(
        OrderId orderId
    ) public inState(orderId, State.Created) {
        require(
            orders[orderId].seller != msg.sender,
            "Seller and buyer must be different"
        );

        orders[orderId].buyer = msg.sender;
        orders[orderId].state = State.Locked;

        orders[orderId].erc20Token.transferFrom(
            orders[orderId].buyer,
            address(this),
            orders[orderId].erc20Price
        );
        orders[orderId].nftToken.transferFrom(
            orders[orderId].seller,
            address(this),
            orders[orderId].nftId
        );

        emit OrderPlaced(orderId, msg.sender);
    }

    function executeOrder(
        OrderId orderId
    ) public onlyBuyer(orderId) inState(orderId, State.Locked) {
        _executeOrder(orderId);

        emit OrderExecuted(orderId);
    }

    function disputeOrderRefund(
        OrderId orderId
    ) public onlyOwner inState(orderId, State.Locked) {
        _refundOrder(orderId);

        emit OrderRefunded(orderId);
    }

    function disputeOrderResolve(
        OrderId orderId
    ) public onlyOwner inState(orderId, State.Locked) {
        _executeOrder(orderId);

        emit OrderExecuted(orderId);
    }

    function _executeOrder(OrderId orderId) internal {
        delete orders[orderId];

        orders[orderId].erc20Token.transfer(
            orders[orderId].seller,
            orders[orderId].erc20Price
        );
        orders[orderId].nftToken.transferFrom(
            address(this),
            orders[orderId].buyer,
            orders[orderId].nftId
        );
    }

    function _refundOrder(OrderId orderId) internal {
        delete orders[orderId];

        orders[orderId].erc20Token.transfer(
            orders[orderId].buyer,
            orders[orderId].erc20Price
        );
        orders[orderId].nftToken.transferFrom(
            address(this),
            orders[orderId].seller,
            orders[orderId].nftId
        );
    }
}
